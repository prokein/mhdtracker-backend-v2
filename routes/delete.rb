###################
#     DELETEs     #
###################

require_relative 'api_version'
require './serializers/serializers'
require './helpers/helpers'
apiVersion = ApiVersion::API_VERSION
helpers RestApiHelpers

# Delete achievement
#
# URL params: driverId [String], key [String]
#
# JSON params: none
#
# @example
#   {
#      "name": "mhdtracker.secka.net:5904/api/v2/achievement?driverId=0uwAQo1P3sMJtoqbChyvOhQM1LS2&key=FIVE_STAR_RIDE",
#      "request": {
#         "url": "mhdtracker.secka.net:5904/api/v2/achievement?driverId=0uwAQo1P3sMJtoqbChyvOhQM1LS2&key=FIVE_STAR_RIDE",
#         "method": "DELETE",
#         "header": [],
#         "body": {},
#         "description": "delete driver's achievement"
#      },
#      "response": [
#			{
#				"id": "459084e9-79ed-426f-bc4f-b50262a6c6c4",
#				"name": "delete achievement response",
#				"originalRequest": {
#					...
#				},
#				"status": "No Content ",
#				"code": 204,
#				"_postman_previewlanguage": "plain",
#				"_postman_previewtype": "text",
#				"header": [
#					{
#						"key": "Connection",
#						"value": "Keep-Alive",
#						"name": "Connection",
#						"description": "Options that are desired for the connection"
#					},
#					...
#				],
#				"cookie": [],
#				"responseTime": 113,
#				"body": ""
#			}
#		]
#   } 
delete "#{apiVersion}/achievement" do
	driver_id = params['driverId']
	key = params['key']
	halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driver_id && key
	achievement = get_driver_by_id(driver_id).achievement.where(key: key)
	achievement.destroy
	status 204
end