###################
#     PUTs     #
###################

require_relative 'api_version'
require './serializers/serializers'
require './helpers/helpers'
apiVersion = ApiVersion::API_VERSION
helpers RestApiHelpers

# Update ride stats
#
# URL params: rideId [String], finished [String]
#
# JSON params: avg_speed [Float], max_speed [Float], duration [Integer], distance [Float], rating [Float]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/ride?rideId=generated_id_of_ride&finished=true",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/ride?rideId=generated_id_of_ride&finished=true",
#           "method": "PUT",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n\t
#                   \"avg_speed\": 45,\n\t
#                   \"max_speed\": 65,\n\t
#                   \"duration\": 184323,\n\t
#                   \"distance\": 150,\n\t
#                   \"rating\": 88\n
#               }"
#           },
#           "description": "update ride stats"
#       },
#       "response": [
#           {
#               "id": "679a9d34-37ed-40fd-b111-c2b83fabe196",
#               "name": "update ride stats response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 142,
#               "body": ""
#           }
#       ]
#   }
put "#{apiVersion}/ride" do
    ride_id = params['rideId']
    finished = params['finished']
    ride = get_ride(ride_id)
    begin
        ride.update_attributes(json_params)
        ride.update_attributes({finished: finished})
        if(finished == true)
            ride.update_attributes({online: false})
        end
    rescue Mongoid::Errors::UnknownAttribute
        halt(422, { message: "Invalid values to update - available: avg_speed max_speed duration distance rating"}.to_json)
    end
    status 200
end

# Set ride finished
#
# URL params: rideId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/ride-finished/generated_id_of_ride",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/ride-finished/generated_id_of_ride",
#           "method": "PUT",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": ""
#           },
#           "description": "set ride atribute finished to true"
#       },
#       "response": [
#           {
#               "id": "cd33750c-fa55-4d1a-a3fa-129c36c4bd51",
#               "name": "mark ride as finished",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 166,
#               "body": ""
#           }
#       ]
#   }
put "#{apiVersion}/ride-finished/:id" do |rideId|
    ride = get_ride(rideId)
    begin
		ride.update_attributes({finished: true})
    rescue Mongoid::Errors::UnknownAttribute
        halt(422, { message: "Invalid values to update!"}.to_json)
    end
    status 201
end

# Update driver stats
#
# URL params: driverId [String]
#
# JSON params: avg_speed [Float], max_speed [Float], rating [FLoat], totalDistance [Float], totalDuration [Integer]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/driver-stats/0uwAQo1P3sMJtoqbChyvOhQM1LS2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/driver-stats/0uwAQo1P3sMJtoqbChyvOhQM1LS2",
#           "method": "PUT",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n\t
#                   \"no_rides\": 2,\n\t
#                   \"avg_speed\": 56,\n\t
#                   \"max_speed\": 100,\n\t
#                   \"rating\": 90,\n\t
#                   \"total_distance\": 254,\n\t
#                   \"total_duration\": 14532\n
#               }"
#           },
#           "description": "update driver stats"
#       },
#       "response": [
#           {
#               "id": "ace72f2a-9cf1-4317-8ba5-1188e5ee76ef",
#               "name": "update driver stats response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 153,
#               "body": ""
#           }
#       ]
#   }
put "#{apiVersion}/driver-stats/:id" do |id|
	driver = get_driver_by_id(id)

    begin
        driver.update_attributes(json_params)
    rescue Mongoid::Errors::UnknownAttribute
        halt(422, { message: "Invalid values to update - available: no_rides avg_speed max_speed rating total_distance total_duration"}.to_json)
    end
	status 200
end

# Update user device token - for notification
#
# URL params: driverId [String], token [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/update-token?driverId=0uwAQo1P3sMJtoqbChyvOhQM1LS2&token=client-token",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/update-token?driverId=0uwAQo1P3sMJtoqbChyvOhQM1LS2&token=client-token",
#           "method": "PUT",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": ""
#           },
#           "description": "Update driver's client app token"
#       },
#       "response": [
#           {
#               "id": "96d9309e-5785-433a-ae6f-c6ac1c9f340e",
#               "name": "update driver token response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 139,
#               "body": ""
#           }
#       ]
#   }
put "#{apiVersion}/update-token" do
    driver_id = params['driverId']
    token = params['token']
	driver = get_driver_by_id(driver_id)

    begin
        driver.update_attribute(:token, token)
    rescue Mongoid::Errors::UnknownAttribute
        halt(422, { message: "Invalid values to update - available: driverId token"}.to_json)
    end
	status 200
end