################
#     GETs     #
################

require_relative 'api_version'
require './serializers/serializers'
require './helpers/helpers'
apiVersion = ApiVersion::API_VERSION
helpers RestApiHelpers

# Init database
#
# URL params: none
#
# JSON params: none
get "#{apiVersion}/init" do
    drivers = Drivers.new
    halt 500, 'Error white init' unless drivers.save
        'MHDTracker init done!'
    status 201
end

# Update links data from mhdspoje
#
# URL params: none
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/update-links",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/update-links",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "update links of KE MHD"
#       },
#       "response": []
#   }
get "#{apiVersion}/update-links" do
    request_uri = 'http://www.mhdspoje.cz/jrw50/php/ListLinkyJSON.php?location=16&packet=80'
    buffer = open(request_uri).read

    # Process string
    links = buffer.split("[[", 2)[1].chomp("]]);").gsub(/\s+/, "").split("],[")
    # Process links
    links.each do |link|
        link = link.delete('"')
        parts = link.split(",")
            
        directions_uri = "http://www.mhdspoje.cz/jrw50/php/ListSmeryJSON.php?linka=#{parts[0]}&location=16&packet=80"
        directionsBuffer = open(directions_uri).read
        directions = directionsBuffer.split("[0,", 2)[1].split('","')
        zeroDirection = directions[0].delete('"').encode('utf-8')
        oneDirection = directions[1].split(',"')[1].encode('utf-8')

        Link.new(id: parts[0].encode('utf-8'), name: parts[1].encode('utf-8'), zeroDirection: zeroDirection, oneDirection: oneDirection).upsert
        sleep 1
    end
    
    status 201
end

# Get latest settings
#
# URL params: none
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/settings",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/settings",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get latest settings"
#       },
#       "response": [
#           {
#               "id": "869a41e5-fffb-44f7-a055-3279caddad78",
#               "name": "settings",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 153,
#               "body": "{
#                   \"version\":1,
#                   \"gps_accuracy\":18,
#                   \"location_update_time\":1000,
#                   \"location_update_dist\":5,
#                   \"low_pass_filter\":true,
#                   \"alpha_low_pass\":0.25,
#                   \"use_average\":true,
#                   \"bus_max_acc\":4.0,
#                   \"bus_min_acc\":1.0,
#                   \"bus_max_break\":5.0,
#                   \"bus_min_break\":1.5,
#                   \"bus_max_turn\":4.0,
#                   \"bus_min_turn\":1.5,
#                   \"bus_min_gyro_turn\":0.22,
#                   \"tram_max_acc\":4.0,
#                   \"tram_min_acc\":1.0,
#                   \"tram_max_break\":5.0,
#                   \"tram_min_break\":1.5,
#                   \"tram_max_turn\":4.0,
#                   \"tram_min_turn\":1.5,
#                   \"tram_min_gyro_turm\":0.22
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/settings" do
    AnalyzeSettingsSerializer.new(Analyzesetting.all.sort(date: -1).limit(1).first).to_json
end 

# Check settings up to date
#
# URL params: version [Integer]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/new-settings/1",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/new-settings/1",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "check if application has latest settings"
#       },
#       "response": [
#           {
#               "id": "9ec3df78-8a24-4a48-b66e-29f94708a56a",
#               "name": "check settings",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 247,
#               "body": "up-to-date"
#           }
#       ]
#   }
get "#{apiVersion}/new-settings/:version" do |version|
    if Analyzesetting.where(:version.gt => version).count > 0
        'need-update'
    else
        'up-to-date'
    end
end

# Get links data
#
# URL params: none
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/get-links",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/get-links",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get links of KE MHD"
#       },
#       "response": [
#           {
#               "id": "e19dab4a-fbea-4c92-a43c-a2215648f54c",
#               "name": "all links",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 163,
#               "body": "[
#                   {
#                       \"id\":\"20013\",
#                       \"name\":\"2\",
#                       \"zd\":\"Stani\\\\u010dn\\\\u00e9 n\\\\u00e1mestie\",
#                       \"od\":\"Havl\\\\u00ed\\\\u010dkova\"
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/get-links" do
    request.secure?
    Link.all.map { |link| LinkSerializer.new(link) }.to_json
end

# Get all drivers
#
# URL params: none
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/drivers",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/drivers",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get all registered drivers"
#       },
#       "response": [
#           {
#               "id": "3f829829-0fd8-48aa-b0e3-698f66a07746",
#               "name": "all drivers",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 185,
#               "body": "[
#                   {
#                       \"id\":\"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",
#                       \"name\":\"Lukáš Prokein\",
#                       \"avg_speed\":2.616257440447926,
#                       \"max_speed\":23.200407028198242,
#                       \"rating\":83.09683251428149,
#                       \"no_rides\":36,
#                       \"total_distance\":60633.0,
#                       \"total_duration\":10047203,
#                       \"achievement\":[
#                           {
#                               \"_id\":{\"$oid\":\"58e0c9a80506098f0989f699\"},
#                               \"date\":\"2017-04-02T09:51:36.021+00:00\",
#                               \"key\":\"FIVE_STAR_RIDE\"
#                           },
#                           ...
#                       ]
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/drivers" do
    Driver.all.map {|driver| DriverSerializer.new(driver)}.to_json
end
		
# Get driver
#
# URL params: driverId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/drivers/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/drivers/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get specified driver"
#       },
#       "response": [
#           {
#               "id": "55bc1f70-efbd-4ac9-b9a5-a82abe4a899f",
#               "name": "driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 169,
#               "body": "{
#                   \"_id\":\"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",
#                   \"achievement\":[
#                       {
#                           \"_id\":{\"$oid\":\"58e0c9a80506098f0989f699\"},
#                           \"date\":\"2017-04-02T09:51:36.021+00:00\",
#                           \"key\":\"FIVE_STAR_RIDE\"
#                       },
#                       ...
#                   ],
#                   \"avg_speed\":2.616257440447926,
#                   \"birthday\":\"1995-03-21\",
#                   \"bus\":true,
#                   \"email\":\"lukas@proke.in\",
#                   \"id\":\"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",
#                   \"max_speed\":23.200407028198242,
#                   \"name\":\"Lukáš Prokein\",
#                   \"no_rides\":36,
#                   \"picture\":\"ad571489-71a8-ed79-9cb2-26ed58b56282\",
#                   \"pictureUrl\":\"https://firebasestorage.googleapis.com/v0/b/mhdtracker-c3fd3.appspot.com/o/users%2Fad571489-71a8-ed79-9cb2-26ed58b56282?alt=media\\u0026token=68abbbf2-53ca-447f-beca-68a5a6db9538\",
#                   \"rating\":83.09683251428149,
#                   \"sex\":\"M\",
#                   \"token\":\"ewNM9eyFJFw:APA91bF_m9htJLxduKYz2eT5cqtVEY2TBT8z0PFYdBztzo2YZHaexCqjGCGMJSEQXflOWZOXbUHDYVFbgAFpd81pm61AcnB-VQfmR8386ZORR-8HD3Q939RomJCKyYrdB0Skhx1BvhTr\",
#                   \"total_distance\":60633.0,
#                   \"total_duration\":10047203,
#                   \"tram\":true
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/drivers/:id" do |id|
	driver = get_driver_by_id(id)
	driver.to_json
end

# Get driver stats
#
# URL params: driverId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/driver-stats/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/driver-stats/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only driver's stats"
#       },
#       "response": [
#           {
#               "id": "d510585f-e905-42d5-8586-a0a364aa7283",
#               "name": "driver stats",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 146,
#               "body": "{
#                   \"avg_speed\":2.616257440447926,
#                   \"max_speed\":23.200407028198242,
#                   \"rating\":83.09683251428149,
#                   \"no_rides\":36,
#                   \"total_distance\":60633.0,
#                   \"total_duration\":10047203
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/driver-stats/:id" do |id|
	driver = get_driver_by_id(id)
	driverStats = DriverStatsSerializer.new(driver)
	driverStats.to_json
end

# Get driver basic info
#
# URL params: driverId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/driver-info/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/driver-info/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only driver's basic info"
#       },
#       "response": [
#           {
#               "id": "3846e8f9-b619-490c-a582-d656fe318551",
#               "name": "driver info",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 157,
#               "body": "{
#                   \"id\":\"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",
#                   \"name\":\"Lukáš Prokein\",
#                   \"pictureUrl\":\"https://firebasestorage.googleapis.com/v0/b/mhdtracker-c3fd3.appspot.com/o/users%2Fad571489-71a8-ed79-9cb2-26ed58b56282?alt=media\\u0026token=68abbbf2-53ca-447f-beca-68a5a6db9538\"
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/driver-info/:id" do |id|
    driver = get_driver_by_id(id)
	driverInfo = DriverInfoSerializer.new(driver)
	driverInfo.to_json
end

# Get all achievements of driver
#
# URL params: driverId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/achievements/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/achievements/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get achievements for specified driver"
#       },
#       "response": [
#           {
#               "id": "0232b216-42a3-492e-86bb-eec83064c051",
#               "name": "achievements of driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 144,
#               "body": "[
#                   {
#                       \"date\":\"2017-04-02T09:51:36.021Z\",
#                       \"key\":\"FIVE_STAR_RIDE\"
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/achievements/:id" do |id|
    achievements = get_driver_by_id(id).achievement
    halt(404, { message: 'Achievements not found'}.to_json) unless achievements
    achievements.map { |achievement| AchievementSerializer.new(achievement) }.to_json
end

# Get all rides of driver
#
# URL params: driverId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides for specified driver"
#       },
#       "response": [
#           {
#               "id": "8f0e704e-9791-497c-a788-8dc7a41a0912",
#               "name": "rides of driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 261,
#               "body": "[
#                   {
#                       \"id\":\"123654-27-0-2017-03-09T09:17:40.962Z\",
#                       \"s\":\"123654\",
#                       \"l\":\"27\",
#                       \"dir\":false,
#                       \"date\":\"2017-03-09T09:17:40.962+00:00\",
#                       \"dur\":575581,
#                       \"dis\":5622.22681903839,
#                       \"max_s\":19.0897579193115,
#                       \"avg_s\":12.8244980055848,
#                       \"r\":99.8442369269885
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides/:id" do |id|
	rides = Ride.where(driver_id: id, finished: true)
	rides.map { |ride| RideSerializer.new(ride) }.to_json
end

# Get full ride
#
# URL params: rideId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/ride/5632-23-0-2017-04-04T15:07:48.058Z",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/ride/5632-23-0-2017-04-04T15:07:48.058Z",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get ride by id"
#       },
#       "response": [
#           {
#               "id": "e9f0163e-ed87-4b1f-a7d9-434f75e4f5b5",
#               "name": "ride",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 172,
#               "body": "{
#                   \"id\":\"5632-23-0-2017-04-04T15:07:48.058Z\",
#                   \"s\":\"5632\",
#                   \"l\":\"23\",
#                   \"dir\":false,
#                   \"date\":\"2017-04-04T13:07:48.058+00:00\",
#                   \"dur\":69595,
#                   \"dis\":43.379916191101074,
#                   \"max_s\":0.0,
#                   \"avg_s\":0.0,
#                   \"r\":77.94883249679712
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/ride/:id" do |id|
    ride = Ride.find(id)
    RideSerializer.new(ride).to_json
end

# Get ride info
#
# URL params: rideId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/ride/info/5632-23-0-2017-04-04T15:07:48.058Z",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/ride/info/5632-23-0-2017-04-04T15:07:48.058Z",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only basic information about specified ride"
#       },
#       "response": [
#           {
#               "id": "9a5f1f4c-ebaf-44e1-a19e-fc855d008974",
#               "name": "info of ride",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 137,
#               "body": "{
#                   \"rideId\":\"5632-23-0-2017-04-04T15:07:48.058Z\",
#                   \"version\":null,
#                   \"date\":\"2017-04-04T13:07:48.058+00:00\",
#                   \"service\":\"5632\",
#                   \"direction\":false,
#                   \"link\":\"23\"
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/ride/info/:id" do |id|
    ride = Ride.find(id)
    RideInfoSerializer.new(ride).to_json
end

# Get ride stats
#
# URL params: rideId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/ride/stats/5632-23-0-2017-04-04T15:07:48.058Z",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/ride/stats/5632-23-0-2017-04-04T15:07:48.058Z",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only stats of specified ride"
#       },
#       "response": [
#           {
#               "id": "964df46b-932d-411e-a966-44d634438800",
#               "name": "stats for ride",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 227,
#               "body": "{
#                   \"avg_speed\":0.0,
#                   \"max_speed\":0.0,
#                   \"duration\":69595,
#                   \"distance\":43.379916191101074,
#                   \"rating\":77.94883249679712
#               }"
#           }
#       ]
#   }
get "#{apiVersion}/ride/stats/:id" do |id|
    ride = Ride.find(id)
    RideStatsSerializer.new(ride).to_json
end

# Get ride waypoints
#
# URL params: rideId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/ride/waypoints/5632-23-0-2017-04-04T15:07:48.058Z",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/ride/waypoints/5632-23-0-2017-04-04T15:07:48.058Z",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get waypoints for ride"
#       },
#       "response": [
#				{
#					"id": "59445a7b-2bb7-4550-9acc-04d96ec7d57e",
#					"name": "waypoints for ride",
#					"originalRequest": {
#						...
#					},
#					"status": "OK ",
#					"code": 200,
#					"_postman_previewlanguage": "html",
#					"_postman_previewtype": "text",
#					"header": [
#						{
#							"key": "Connection",
#							"value": "Keep-Alive",
#							"name": "Connection",
#							"description": "Options that are desired for the connection"
#						},
#						...
#					],
#					"cookie": [],
#					"responseTime": 353,
#					"body": "[
#                       {
#                           \"n\":0,
#                           \"r\":76.71146111949942,
#                           \"t\":43,
#                           \"t_a\":0.0,
#                           \"a\":{
#                               \"a_x\":0.9172095656394958,
#                               \"a_y\":-0.5399462413012978,
#                               \"a_z\":1.6986561664150173
#                           },
#                           \"g\":{
#                               \"g_x\":-0.2480112910270691,
#                               \"g_y\":0.15974126756191254,
#                               \"g_z\":-0.3194825351238251
#                           },
#                           \"l\":{
#                               \"a\":361.97204158552654,
#                               \"b\":0.0,
#                               \"la\":48.73095662114012,
#                               \"lo\":21.243999292079952,
#                               \"s\":0.0
#                           }
#                       },
#                       ...
#                   ]
#				}
#			]
#   }
get "#{apiVersion}/ride/waypoints/:id" do |id|
    waypoints = Waypoint.where(ride_id: id)
    waypoints.map { |waypoint| WaypointSerializer.new(waypoint) }.to_json
end

# Get rides of the day
#
# URL params: date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day/2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day/2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides of the day"
#       },
#       "response": [
#           {
#               "id": "65e440c6-4b5c-433a-88f6-4e11c31f9ce6",
#               "name": "rides of the day",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 171,
#               "body": "[
#                   {
#                       \"id\":\"1215-23-1-2017-05-12T12:07:30.746Z\",
#                       \"s\":\"1215\",
#                       \"l\":\"23\",
#                       \"dir\":true,
#                       \"date\":\"2017-05-12T10:07:30.746+00:00\",
#                       \"dur\":26053,
#                       \"dis\":0.0,
#                       \"max_s\":0.0,
#                       \"avg_s\":0.0,
#                       \"r\":100.0
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-day/:date" do |date|
    date = Date.parse date
    startDate = Date.new(date.year, date.mon, date.mday)
    endDate = Date.new(date.year, date.mon, date.mday + 1)
	rides = Ride.where(:date.gte => startDate, :date.lt => endDate, finished: true).order_by(:date.asc)
	rides.map { |ride| RideSerializer.new(ride) }.to_json
end
    
# Get rides of the day for driver
#
# URL params: driverId [String], date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day-for-driver?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day-for-driver?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides of the day for driver"
#       },
#       "response": [
#           {
#               "id": "568a98b5-cdb7-4cd5-8908-6ef2025f95c4",
#               "name": "rides of the day for driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 261,
#               "body": "[
#                   {
#                       \"id\":\"1215-23-1-2017-05-12T12:07:30.746Z\",
#                       \"s\":\"1215\",
#                       \"l\":\"23\",
#                       \"dir\":true,
#                       \"date\":\"2017-05-12T10:07:30.746+00:00\",
#                       \"dur\":26053,
#                       \"dis\":0.0,
#                       \"max_s\":0.0,
#                       \"avg_s\":0.0,
#                       \"r\":100.0
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-day-for-driver" do
    driverId = params["driverId"]
    date = Date.parse(params['date'])
    halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driverId && date
    startDate = Date.new(date.year, date.mon, date.mday)
    endDate = Date.new(date.year, date.mon, date.mday + 1)
	rides = Ride.where(driver_id: driverId , :date.gte => startDate, :date.lt => endDate).order_by(:date.asc)
	rides.map { |ride| RideSerializer.new(ride) }.to_json
end

# Get ride stats of the day
#
# URL params: date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day/stats/2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day/stats/2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only stats from rides of the day"
#       },
#       "response": [
#           {
#               "id": "7650adfb-a48b-4422-be1a-d42029d4074b",
#               "name": "stats of rides of the day",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 169,
#               "body": "[
#                   {
#                       \"avg_speed\":0.0,
#                       \"max_speed\":0.0,
#                       \"duration\":26053,
#                       \"distance\":0.0,
#                       \"rating\":100.0
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-day/stats/:date" do |date|
	date = Date.parse date
	startDate = Date.new(date.year, date.mon, date.mday)
    endDate = Date.new(date.year, date.mon, date.mday + 1)
	rides = Ride.where(:date.gte => startDate, :date.lt => endDate).order_by(:date.asc)
	rides.map { |ride| RideStatsSerializer.new(ride) }.to_json
end

# Get ride stats of the day for driver
#
# URL params: driverId [String], date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day-for-driver/stats?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day-for-driver/stats?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get stats of rides for driver"
#       },
#       "response": [
#           {
#               "id": "62525154-e68b-4ad9-858a-5f5a91b7118e",
#               "name": "stats of rides of the day for driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 183,
#               "body": "[
#                   {
#                       \"avg_speed\":0.0,
#                       \"max_speed\":0.0,
#                       \"duration\":26053,
#                       \"distance\":0.0,
#                       \"rating\":100.0
#                   },
#                   ...
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-day-for-driver/stats" do
    driverId = params['driverId']
	date = Date.parse params['date']
    halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driverId && date
	startDate = Date.new(date.year, date.mon, date.mday)
    endDate = Date.new(date.year, date.mon, date.mday + 1)
	rides = Ride.where(driver_id: driverId, :date.gte => startDate, :date.lt => endDate).order_by(:date.asc)
	rides.map { |ride| RideStatsSerializer.new(ride) }.to_json
end

# Get rides of the week
#
# URL params: date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week/2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week/2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides of the week"
#       },
#       "response": [
#           {
#               "id": "80907b13-6e48-4344-97c0-1d7fc0d36d4a",
#               "name": "rides of the week",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 181,
#               "body": "[
#                   {
#                       \"id\":\"43610-16-1-2017-05-08T16:32:14.322Z\",
#                       \"s\":\"43610\",
#                       \"l\":\"16\",
#                       \"dir\":true,
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"dur\":8692,
#                       \"dis\":0.0,
#                       \"max_s\":0.0,
#                       \"avg_s\":0.0,
#                       \"r\":85.92123885949452
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-week/:date" do |date|
	date = Date.parse date
	rides = Ride.between(date: (date.beginning_of_week..date.end_of_week))
	rides.map { |ride| RideSerializer.new(ride) }.to_json
end

# Get rides of the week for driver
#
# URL params: driverId [String], date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week-for-driver?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week-for-driver?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides of the week for driver"
#       },
#       "response": [
#           {
#               "id": "311d8fdb-09e5-4ed6-a767-e0c48c787279",
#               "name": "rides of the week for driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 525,
#               "body": "[
#                   {
#                       \"id\":\"43610-16-1-2017-05-08T16:32:14.322Z\",
#                       \"s\":\"43610\",
#                       \"l\":\"16\",
#                       \"dir\":true,
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"dur\":8692,
#                       \"dis\":0.0,
#                       \"max_s\":0.0,
#                       \"avg_s\":0.0,
#                       \"r\":85.92123885949452
#                   },
#               ]
#           }
#       ]
#   }

get "#{apiVersion}/rides-of-the-week-for-driver" do
	driverId = params['driverId']
	date = Date.parse params['date']
    halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driverId && date
	rides = Ride.where(driver_id: driverId).between(date: (date.beginning_of_week..date.end_of_week))
    rides.map { |ride| RideSerializer.new(ride) }.to_json
end

# Get ride stats of the week
#
# URL params: date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week/stats/2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week/stats/2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get stats of rides of the week"
#       },
#       "response": [
#           {
#               "id": "8c30b486-5861-4fb7-8aad-ca46c6e4b980",
#               "name": "stats of rides of the week",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 175,
#               "body": "[
#                   {
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"rating\":85.92123885949452
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-week/stats/:date" do |date|
	date = Date.parse date
	rides = Ride.between(date: (date.beginning_of_week..date.end_of_week))
	rides.map { |ride| RideChartSerializer.new(ride) }.to_json
end

# Get ride stats of the week for driver
#
# URL params: driverId [String], date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-week-for-driver/stats?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-day-for-driver/stats?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only stats of rides of the week for driver"
#       },
#       "response": [
#           {
#               "id": "3ed3ebd1-1008-4171-a202-419243d09e99",
#               "name": "stats of rides of the week for driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 167,
#               "body": "[
#                   {
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"rating\":85.92123885949452
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-week-for-driver/stats" do
	driverId = params['driverId']
	date = Date.parse params['date']
    halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driverId && date
	rides = Ride.where(driver_id: driverId).between(date: (date.beginning_of_week..date.end_of_week))
	rides.map { |ride| RideChartSerializer.new(ride) }.to_json
end

# Get rides of the month
#
# URL params: date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month/2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month/2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides of the month"
#       },
#       "response": [
#           {
#               "id": "65287074-bc5b-4e32-87f9-ff88a68b4558",
#               "name": "redes of the month",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 263,
#               "body": "[
#                   {
#                       \"id\":\"43610-16-1-2017-05-08T16:32:14.322Z\",
#                       \"s\":\"43610\",
#                       \"l\":\"16\",
#                       \"dir\":true,
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"dur\":8692,
#                       \"dis\":0.0,
#                       \"max_s\":0.0,
#                       \"avg_s\":0.0,
#                       \"r\":85.92123885949452
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-month/:date" do |date|
	date = Date.parse date
	rides = Ride.between(date: (date.beginning_of_month..date.end_of_month))
	rides.map { |ride| RideSerializer.new(ride) }.to_json
end

# Get rides of the month for driver
#
# URL params: driverId [String], date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month-for-driver?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month-for-driver?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get rides of the month for driver"
#       },
#       "response": [
#           {
#               "id": "258012e3-aa05-4824-869b-dac7fabf2469",
#               "name": "rides of the month for driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 174,
#               "body": "[
#                   {
#                       \"id\":\"43610-16-1-2017-05-08T16:32:14.322Z\",
#                       \"s\":\"43610\",
#                       \"l\":\"16\",
#                       \"dir\":true,
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"dur\":8692,
#                       \"dis\":0.0,
#                       \"max_s\":0.0,
#                       \"avg_s\":0.0,
#                       \"r\":85.92123885949452
#                   },
#                   ...
#               ]    
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-month-for-driver" do 
	driverId = params['driverId']
	date = Date.parse params['date']
    halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driverId && date
	rides = Ride.where(driver_id: driverId).between(date: (date.beginning_of_month..date.end_of_month))
	rides.map { |ride| RideSerializer.new(ride) }.to_json
end

# Get ride stats of the month
#
# URL params: date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month/stats/2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month/stats/2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only stats of rides of the month"
#       },
#       "response": [
#           {
#               "id": "619a3c1f-650a-4a3c-aee9-3d8648742101",
#               "name": "stats of rides of the month",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 164,
#               "body": "[
#                   {
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"rating\":85.92123885949452
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-month/stats/:date" do |date|
	date = Date.parse date
	rides = Ride.between(date: (date.beginning_of_month..date.end_of_month))
	rides.map { |ride| RideChartSerializer.new(ride) }.to_json
end

# Get ride stats of the month for driver
#
# URL params: driverId [String], date [DateTime]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month-for-driver/stats?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/rides-of-the-month-for-driver/stats?driverId=qz9s6qaVmXQw1kMyrmlbpGXVmHB2&date=2017-05-12",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get only stats of rides of the month for driver"
#       },
#       "response": [
#           {
#               "id": "8ce01d0d-97f2-4b59-aa30-85ba288af929",
#               "name": "rides of the month for driver",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 424,
#               "body": "[
#                   {
#                       \"date\":\"2017-05-08T14:32:14.322+00:00\",
#                       \"rating\":85.92123885949452
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/rides-of-the-month-for-driver/stats" do
	driverId = params['driverId']
	date = Date.parse params['date']
    halt(400, { message: "Invalid url parameters - require driverId rideId"}.to_json) unless driverId && date
	rides = Ride.where(driver_id: driverId).between(date: (date.beginning_of_month..date.end_of_month))
	rides.map { |ride| RideChartSerializer.new(ride) }.to_json
end

# Get warnings for ride
#
# URL params: rideId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/warnigs-for-ride/4258-10-1-2017-05-12T12:59:47.672Z",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/warnigs-for-ride/4258-10-1-2017-05-12T12:59:47.672Z",
#           "method": "GET",
#           "header": [],
#           "body": {},
#           "description": "get warnings for ride"
#       },
#       "response": [
#           {
#               "id": "2c40a7b5-32ff-4750-9924-b2bdfa96347b",
#               "name": "warnings for ride",
#               "originalRequest": {
#                   ...
#               },
#               "status": "OK ",
#               "code": 200,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                   "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 144,
#               "body": "[
#                   {
#                       \"id\":{\"$oid\":\"591595ada5ea5e6bcd3386bb\"},
#                       \"ride_id\":\"4258-10-1-2017-05-12T12:59:47.672Z\",
#                       \"key\":\"ACCELERATION\",
#                       \"waypoint\":{
#                           \"n\":null,
#                           \"r\":null,
#                           \"t\":null,
#                           \"t_a\":null,
#                           \"a\":null,
#                           \"g\":null,
#                           \"l\":null
#                       }
#                   },
#                   ...
#               ]
#           }
#       ]
#   }
get "#{apiVersion}/warnigs-for-ride/:id" do |rideId|
    warnings = Ridewarning.where(ride_id: rideId)
    warnings.map { |warning| WarningSerializer.new(warning) }.to_json
end