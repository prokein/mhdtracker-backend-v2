#module in which is defined version of current REST API
module ApiVersion
  #api version constant - part of requests urls
  API_VERSION = '/api/v2'
end