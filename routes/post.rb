#################
#     POSTs     #
#################

require_relative 'api_version'
require './serializers/serializers'
require './helpers/helpers'
apiVersion = ApiVersion::API_VERSION
helpers RestApiHelpers

# Create settings
#
# URL params: none
#
# JSON params: version [Integer], gps_accuracy [Float], location_update_time [Integer], location_update_dist [Float], bus_max_acc [Float],
# bus_min_acc [Float], bus_max_break [Float], bus_min_break [Float], bus_max_turn [Float], bus_min_turn [Float], bus_min_gyro_turn [Float], tram_max_acc [Float], tram_min_acc [Float], tram_max_break [Float], tram_min_break [Float],
# tram_max_turn [Float], tram_min_turn [Float], tram_min_gyro_turn [Float]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/create-settings",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/create-settings",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n    
#                           \"date\" : \"2017-04-15T13:26:17.295Z\",\n
#                           \"version\" : 2,\n    \"gps_accuracy\" : 18,\n
#                           \"location_update_time\" : 1000,\n
#                           \"location_update_dist\" : 5,\n
#                           \"low_pass_filter\":true,
#                           \"alpha_low_pass\":0.25,
#                           \"use_average\":true,
#                           \"bus_max_acc\" : 4,\n
#                           \"bus_min_acc\" : 1,\n
#                           \"bus_max_break\" : 5,\n
#                           \"bus_min_break\" : 1.5,\n
#                           \"bus_max_turn\" : 4,\n
#                           \"bus_min_turn\" : 1.5,\n
#                           \"bus_min_gyro_turn\" : 0.22,\n
#                           \"tram_max_acc\" : 4,\n
#                           \"tram_min_acc\" : 1,\n
#                           \"tram_max_break\" : 5,\n
#                           \"tram_min_break\" : 1.5,\n
#                           \"tram_max_turn\" : 4,\n
#                           \"tram_min_turn\" : 1.5,\n
#                           \"tram_min_gyro_turn\" : 0.22\n
#                    }"
#           },
#           "description": "create new settings"
#       },
#       "response": [
#           {
#               "id": "bbcd606a-d6b1-43b8-b268-a1d4840b6381",
#               "name": "create new settings response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": ""
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 169,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/create-settings" do
    params = json_params
    version = params['version']
    gps_accuracy = params['gps_accuracy']
    location_update_time = params['location_update_time']
    location_update_dist = params['location_update_dist']
    bus_max_acc = params['bus_max_acc']
    bus_min_acc = params['bus_min_acc']
    bus_max_break = params['bus_max_break']
    bus_min_break = params['bus_min_break']
    bus_max_turn = params['bus_max_turn']
    bus_min_turn = params['bus_min_turn']
    bus_min_gyro_turn = params['bus_min_gyro_turn']
    tram_max_acc = params['tram_max_acc']
    tram_min_acc = params['tram_min_acc']
    tram_max_break = params['tram_max_break']
    tram_min_break = params['tram_min_break']
    tram_max_turn = params['tram_max_turn']
    tram_min_turn = params['tram_min_turn']
    tram_min_gyro_turn = params['tram_min_gyro_turn']
    halt(400, { message: "Invalid json parameters - require version gps_accuracy lcoation_update_time location_update_dist 
                        bus_max_acc bus_min_acc bus_max_break bus_min_break bus_max_turn bus_min_turn bus_min_gyro_turn tram_max_acc tram_min_acc tram_max_break tram_min_break 
                        tram_max_turn tram_min_turn tram_min_gyro_turn"}.to_json) unless version &&
                        gps_accuracy && location_update_time && location_update_dist && bus_max_acc && bus_min_acc && bus_max_break && bus_min_break &&
                        bus_max_turn && bus_min_turn && bus_min_gyro_turn && tram_max_acc && tram_min_acc && tram_max_break && tram_min_break &&
                        tram_max_turn && tram_min_turn && tram_min_gyro_turn
    Analyzesetting.create!({date: Time.now, version: version, gps_accuracy: gps_accuracy, location_update_time: location_update_time,
                        location_update_dist: location_update_dist, bus_max_acc: bus_max_acc, bus_min_acc: bus_min_acc, 
                        bus_max_break: bus_max_break, bus_min_break: bus_min_break, bus_max_turn: bus_max_turn, bus_min_turn: bus_min_turn,
                        bus_min_gyro_turn: bus_min_gyro_turn, tram_max_acc: tram_max_acc, tram_min_acc: tram_min_acc, tram_max_break: tram_max_break, tram_min_break: tram_min_break,
                        tram_max_turn: tram_max_turn, tram_min_turn: tram_min_turn, tram_min_gyro_turn: tram_min_gyro_turn})
    status 201
end

# Create warning
#
# URL params: rideId [String], key [String]
#
# JSON params: waypoint [Waypoint]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/create-warning?rideId=generated_id_of_ride&key=ACCELERATION",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/create-warning?rideId=generated_id_of_ride&key=ACCELERATION",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n    
#                   \"l\" : {\n
#                       \"a\" : 284.252697408831,\n
#                       \"b\" : 310.522644042969,\n
#                       \"la\" : 48.7584618838263,\n
#                       \"lo\" : 21.2619108417025,\n
#                       \"s\" : 3.22450137138367\n
#                   },\n
#                   \"t_a\" : 0.0,\n
#                   \"t\" : 0,\n
#                   \"r\" : 61.5711609522502,\n
#                   \"n\" : 0\n
#               }"
#           },
#           "description": "create warning for ride"
#       },
#       "response": [
#           {
#               "id": "642b454d-60f5-446e-a822-1be098787c39",
#               "name": "create warning response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 278,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/create-warning" do
    rideId = params['rideId']
    key = params['key']
    halt(400, { message: "Invalid url parameters - require rideId key"}.to_json) unless rideId && key
    waypoint = json_params
    Ridewarning.create!({key: key, ride_id: rideId, waypoint: waypoint})
    status 201
end

# Create driver
#
# URL params: none
#
# JSON params: id [String], name [String]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/create-driver",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/create-driver",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n\t\"id\" : \"some_id\",\n\t\"name\" : \"Dominik Secka\"\n}"
#           },
#           "description": "Create driver"
#       },
#       "response": [
#           {
#               "id": "8eec8763-a3f5-4475-a669-1a360890c6bc",
#               "name": "create new driver response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 186,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/create-driver" do
    params = json_params

    Driver.create!(params)

    response.headers['Location'] = "#{base_url}/api/v2/drivers/#{params['id']}"
    status 201
end

# Create overflow report
#
# URL params: none
#
# JSON params: waypoint [Waypoint], overflown_rating [Float], driver_id [String], ride_id [String], event_key [String]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/add-overflow-report",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/add-overflow-report",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n
#                           \"event_key\" : \"ACCELERATION\",\n
#                           \"waypoint\" : {\n
#                               \"a\" : {\n
#                                   \"a_x\" : -3.35161399841309,\n
#                                   \"a_y\" : -5.9095025062561,\n
#                                   \"a_z\" : 4.17798757553101\n
#                               },\n
#                               \"g\" : {\n
#                                   \"g_x\" : 0.0485637858510017,\n
#                                   \"g_y\" : 0.244956970214844,\n
#                                   \"g_z\" : 0.0736092627048492\n
#                               },\n
#                               \"l\" : {\n
#                                   \"a\" : 248.950612837815,\n
#                                   \"b\" : 183.213226318359,\n
#                                   \"la\" : 48.7284034667818,\n
#                                   \"lo\" : 21.2772652001752,\n
#                                   \"s\" : 12.1223850250244\n
#                               },\n
#                               \"t_a\" : 7.97565774385393,\n
#                               \"t\" : 118782,\n
#                               \"r\" : 0.0,\n
#                               \"n\" : 101\n
#                           },\n
#                           \"ride_id\" : \"6980-10-0-2017-05-11T15:17:15.864Z\",\n
#                           \"driver_id\" : \"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",\n
#                           \"overflown_rating\" : -5.93291918436687\n
#               }"
#           },
#           "description": "add new overflow report"
#       },
#       "response": [
#           {
#               "id": "c036155a-da6f-4231-a9e0-828d4780794b",
#               "name": "add overflow report response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 165,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/add-overflow-report" do
    params = json_params
    Report.create!(params)
    status 201
end

# Create overflow reports
#
# URL params: none
#
# JSON params: [Array] of - waypoint [Waypoint], overflown_rating [Float], driver_id [String], ride_id [String], event_key [String]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/add-overflow-reports",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/add-overflow-reports",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "[\n\t
#                           {\n\t
#                               \"event_key\" : \"ACCELERATION\",\n\t
#                               \"waypoint\" : {\n\t
#                                   \"a\" : {\n\t
#                                       \"a_x\" : -3.35161399841309,\n\t
#                                       \"a_y\" : -5.9095025062561,\n\t
#                                       \"a_z\" : 4.17798757553101\n\t
#                                   },\n\t
#                                   \"g\" : {\n\t
#                                       \"g_x\" : 0.0485637858510017,\n\t
#                                       \"g_y\" : 0.244956970214844,\n\t
#                                       \"g_z\" : 0.0736092627048492\n\t
#                                   },\n\t
#                                   \"l\" : {\n\t
#                                       \"a\" : 248.950612837815,\n\t
#                                       \"b\" : 183.213226318359,\n\t
#                                       \"la\" : 48.7284034667818,\n\t
#                                       \"lo\" : 21.2772652001752,\n\t
#                                       \"s\" : 12.1223850250244\n\t
#                                   },\n\t
#                                   \"t_a\" : 7.97565774385393,\n\t
#                                   \"t\" : 118782,\n\t
#                                   \"r\" : 0.0,\n\t
#                                   \"n\" : 101\n\t
#                               },\n\t
#                               \"ride_id\" : \"6980-10-0-2017-05-11T15:17:15.864Z\",\n\t
#                               \"driver_id\" : \"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",\n\t
#                               \"overflown_rating\" : -5.93291918436687\n\t
#                           },\n\t
#                           {\n\t
#                               \"event_key\" : \"BREAKING\",\n\t
#                               \"waypoint\" : {\n\t
#                                   \"a\" : {\n\t
#                                       \"a_x\" : 3.19913125038147,\n\t
#                                       \"a_y\" : -5.03771114349365,\n\t
#                                       \"a_z\" : -5.21091938018799\n\t
#                                   },\n\t
#                                   \"g\" : {\n\t
#                                       \"g_x\" : 0.119424156844616,\n\t
#                                       \"g_y\" : -0.0907134860754013,\n\t
#                                       \"g_z\" : 0.0714712366461754\n\t
#                                   },\n\t
#                                   \"l\" : {\n\t
#                                       \"a\" : 277.026420450783,\n\t
#                                       \"b\" : 183.364730834961,\n\t
#                                       \"la\" : 48.6994041253726,\n\t
#                                       \"lo\" : 21.234235226688,\n\t
#                                       \"s\" : 10.4014225006104\n\t
#                                   },\n\t
#                                   \"t_a\" : 7.92254095030541,\n\t
#                                   \"t\" : 275727,\n\t
#                                   \"r\" : 0.0,\n\t
#                                   \"n\" : 230\n\t
#                               },\n\t
#                               \"ride_id\" : \"3908-10-0-2017-05-11T15:35:44.122Z\",\n\t
#                               \"driver_id\" : \"qz9s6qaVmXQw1kMyrmlbpGXVmHB2\",\n\t
#                               \"overflown_rating\" : -6.02626800537109\n\t}\n
#                   ]"
#           },
#           "description": "add multiple overflow reports"
#       },
#       "response": [
#           {
#               "id": "f9afba33-3824-4526-9d84-f695b895655f",
#               "name": "add overflow reports response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 187,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/add-overflow-reports" do
    jparams = json_params
    jparams.each do |report|
        waypoint = report["waypoint"]
        overflown_rating = report["overflown_rating"]
        driver_id = report["driver_id"]
        ride_id = report["ride_id"]
        event_key = report["event_key"]
        halt(400, { message: "Invalid json parameters - waypoint overflown_rating driver_id ride_id event_key"}.to_json) unless waypoint && overflown_rating && driver_id && ride_id && event_key
        Report.create!(event_key: event_key, driver_id: driver_id, overflown_rating: overflown_rating, waypoint: waypoint)
    end
	status 201
end

# Init ride
#
# URL params: driverid [String]
#
# JSON params: rideId [String], date [DateTime], service [String], direction [Boolean], link [String], version [Integer],
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/init-ride/0uwAQo1P3sMJtoqbChyvOhQM1LS2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/init-ride/0uwAQo1P3sMJtoqbChyvOhQM1LS2",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n\t
#                   \"id\": \"generated_id_of_ride\",\n
#                   \"date\": \"2017-04-14 11:57:37.484Z\",\n
#                   \"s\": \"22\",\n
#                   \"dir\": true,\n
#                   \"l\": \"3\",\n
#                   \"v\": 1\n
#               }\n"
#           },
#           "description": "initialization of ride"
#       },
#       "response": [
#           {
#               "id": "b5180874-5efb-4a27-8e44-3d9c9f28a73c",
#               "name": "initialization of ride",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 174,
#               "body": "id_of_ride"
#           }
#       ]
#   }
post "#{apiVersion}/init-ride/:id" do |id|
    params = json_params
    rideId = params['id']
    date = DateTime.parse(params['date'])
    service = params['s']
    direction = params['dir']
    link = params['l']
    version = params['v']
    halt(400, { message: "Invalid json parameters - require id date s dir l v"}.to_json) unless rideId && date && service && !direction.nil? && link && version

    Ride.create!(id: rideId, driver_id: id, date: date, service: service, direction: direction, link: link, version: version)
    status 201
    body rideId
end

# Add achievement
#
# URL params: driverid [String]
#
# JSON params: key, date
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/add-achievement/0uwAQo1P3sMJtoqbChyvOhQM1LS2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/add-achievement/0uwAQo1P3sMJtoqbChyvOhQM1LS2",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n
#                   \"key\" : \"FIVE_STAR_RIDE\",\n
#                   \"date\" : \"2017-04-02T09:51:36.021Z\"\n
#               }"
#           },
#           "description": "add achievement for driver"
#       },
#       "response": [
#           {
#               "id": "79d59dca-2abb-46d1-a958-3d97092a1e9d",
#               "name": "add achievement for driver response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 229,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/add-achievement/:id" do |driver_id|
    params = json_params
    key = params['key']
    date = params['date']
    halt(400, { message: "Invalid json parameters - require key date"}.to_json) unless key && date
    driver = get_driver_by_id(driver_id)
	driver.achievement << Achievement.new(key: key, date: date)
	driver.save!
    status 201
end

# Create waypoint
#
# URL params: rideId [String]
#
# JSON params: no [Integer], rating [Float], timestamp [Integer], total_acceleration [Float], acceleration [Acceleration], gyro [Gyro], location [Location]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/add-waypoint/generated_id_of_ride",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/add-waypoint/generated_id_of_ride",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "{\n\t
#                   \"n\": 6,\n\t
#                   \"r\": 94.7323018720446,\n\t
#                   \"t\": 6654,\n\t
#                   \"t_a\": 1.00588771935078,\n\t
#                   \"a\": {\n\t\t
#                       \"a_x\": -0.39917597733438,\n\t\t
#                       \"a_y\": 0.378814541048963, \n\t\t
#                       \"a_z\": 0.532610237752934\n\t
#                   },\n\t
#                   \"g\": {\n\t\t
#                       \"g_x\": 0.00874092688318342,\n\t\t
#                       \"g_y\": 0.322145341878588,\n\t\t
#                       \"g_z\": 0.157172851055988\n\t
#                   },\n\t
#                   \"l\": {\n\t\t
#                       \"a\": 317.088174101001,\n\t\t
#                       \"b\": 208.271499633789,\n\t\t
#                       \"la\": 48.7599871183488,\n\t\t
#                       \"lo\": 21.2594602142051,\n\t\t
#                       \"s\": 9.73146915435791\n\t}\n
#                   }"
#           },
#           "description": "add new waypoint"
#       },
#       "response": [
#           {
#               "id": "dbeac8ed-c47a-48a1-9db8-1292c99dedad",
#               "name": "add new waypoint response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 213,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/add-waypoint/:id" do |rideId|
    jparams = json_params
	no = jparams["n"]
	rating = jparams["r"]
	timestamp = jparams["t"]
	total_acceleration = jparams["t_a"]
	accelerationParams = jparams["a"]
	gyroParams = jparams["g"]
	locationParams = jparams["l"]
    halt(400, { message: "Invalid json parameters - require n r t t_a a g l"}.to_json) unless no && rating && timestamp && total_acceleration && accelerationParams && gyroParams && locationParams 

    acceleration = Acceleration.new(accX: accelerationParams["a_x"], accY: accelerationParams["a_y"], accZ: accelerationParams["a_z"])
    gyro = Gyro.new(gyroX: gyroParams["g_x"], gyroY: gyroParams["g_y"], gyroZ: gyroParams["g_z"])
    location = Location.new(altitude: locationParams["a"], bearing: locationParams["b"], latitude: locationParams["la"], longitude: locationParams["lo"], speed: locationParams["s"])
	    
	Waypoint.create!(ride_id: rideId, no: no, rating: rating, timestamp: timestamp, total_acceleration: total_acceleration, acceleration: acceleration, gyro: gyro, location: location)
	ride = get_ride(rideId)
    ride.update_attributes({online: true})
    status 201
end

# Create waypoints
#
# URL params: rideId [String]
#
# JSON params: [Array] of - no [Integer], rating [Float], timestamp [Integer], total_acceleration [Float], acceleration [Acceleration], gyro [Gyro], location [Location]
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/add-waypoints/generated_id_of_ride",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/add-waypoints/generated_id_of_ride",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": "[\n\t
#                   {\n\t\t
#                       \"n\": 6,\n\t\t
#                       \"r\": 94.7323018720446,\n\t\t
#                       \"t\": 6654,\n\t\t
#                       \"t_a\": 1.00588771935078,\n\t\t
#                       \"a\": {\n\t\t\t
#                           \"a_x\": -0.39917597733438,\n\t\t\t
#                           \"a_y\": 0.378814541048963, \n\t\t\t
#                           \"a_z\": 0.532610237752934\n\t\t
#                       },\n\t\t
#                       \"g\": {\n\t\t\t
#                           \"g_x\": 0.00874092688318342,\n\t\t\t
#                           \"g_y\": 0.322145341878588,\n\t\t\t
#                           \"g_z\": 0.157172851055988\n\t\t
#                       },\n\t\t
#                       \"l\": {\n\t\t\t
#                           \"a\": 317.088174101001,\n\t\t\t
#                           \"b\": 208.271499633789,\n\t\t\t
#                           \"la\": 48.7599871183488,\n\t\t\t
#                           \"lo\": 21.2594602142051,\n\t\t\t
#                           \"s\": 9.73146915435791\n\t\t
#                       }\n\t
#                   },\n\t
#                   {\n\t\t
#                       \"n\": 7,\n\t\t
#                       \"r\": 94.7323018720446,\n\t\t
#                       \"t\": 6654,\n\t\t
#                       \"t_a\": 1.00588771935078,\n\t\t
#                       \"a\": {\n\t\t\t
#                           \"a_x\": -0.39917597733438,\n\t\t\t
#                           \"a_y\": 0.378814541048963, \n\t\t\t
#                           \"a_z\": 0.532610237752934\n\t\t
#                       },\n\t\t
#                       \"g\": {\n\t\t\t
#                           \"g_x\": 0.00874092688318342,\n\t\t\t
#                           \"g_y\": 0.322145341878588,\n\t\t\t
#                           \"g_z\": 0.157172851055988\n\t\t
#                       },\n\t\t
#                       \"l\": {\n\t\t\t
#                           \"a\": 317.088174101001,\n\t\t\t
#                           \"b\": 208.271499633789,\n\t\t\t
#                           \"la\": 48.7599871183488,\n\t\t\t
#                           \"lo\": 21.2594602142051,\n\t\t\t
#                           \"s\": 9.73146915435791\n\t\t
#                       }\n\t
#                   }\n
#               ]"
#           },
#           "description": "add multiple waypoints"
#       },
#       "response": [
#           {
#               "id": "6de5bedf-60b9-4c28-a6ac-ad2af6ea2a0b",
#               "name": "add multiple waypoints response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 186,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/add-waypoints/:id" do |rideId|
    jparams = json_params
    jparams.each do |waypoint|
        no = waypoint["n"]
        rating = waypoint["r"]
        timestamp = waypoint["t"]
        total_acceleration = waypoint["t_a"]

        accelerationParams = waypoint["a"]
        gyroParams = waypoint["g"]
        locationParams = waypoint["l"]
            
        halt(400, { message: "Invalid json parameters - require n r t t_a a g l"}.to_json) unless no && rating && timestamp && total_acceleration && accelerationParams && gyroParams && locationParams 

        acceleration = Acceleration.new(accX: accelerationParams["a_x"], accY: accelerationParams["a_y"], accZ: accelerationParams["a_z"])
        gyro = Gyro.new(gyroX: gyroParams["g_x"], gyroY: gyroParams["g_y"], gyroZ: gyroParams["g_z"])
        location = Location.new(altitude: locationParams["a"], bearing: locationParams["b"], latitude: locationParams["la"], longitude: locationParams["lo"], speed: locationParams["s"])
            
        Waypoint.create!(ride_id: rideId, no: no, rating: rating, timestamp: timestamp, total_acceleration: total_acceleration, acceleration: acceleration, gyro: gyro, location: location)
        ride = get_ride(rideId)
        ride.update_attributes({online: false})
    end
	status 201
end

# Check achieved achievements
#
# URL params: driverId [String]
#
# JSON params: none
#
# @example
#   {
#       "name": "mhdtracker.secka.net:5904/api/v2/are-achieved/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#       "request": {
#           "url": "mhdtracker.secka.net:5904/api/v2/are-achieved/qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#           "method": "POST",
#           "header": [
#               {
#                   "key": "Content-Type",
#                   "value": "application/json",
#                   "description": ""
#               }
#           ],
#           "body": {
#               "mode": "raw",
#               "raw": ""
#           },
#           "description": "check for achieved achievements for driver"
#       },
#       "response": [
#           {
#               "id": "17079274-ca61-4514-a703-25f4ef1c5b21",
#               "name": "check achieved response",
#               "originalRequest": {
#                   ...
#               },
#               "status": "Created ",
#               "code": 201,
#               "_postman_previewlanguage": "html",
#               "_postman_previewtype": "text",
#               "header": [
#                   {
#                       "key": "Connection",
#                       "value": "Keep-Alive",
#                       "name": "Connection",
#                       "description": "Options that are desired for the connection"
#                   },
#                   ...
#               ],
#               "cookie": [],
#               "responseTime": 272,
#               "body": ""
#           }
#       ]
#   }
post "#{apiVersion}/are-achieved/:driverId" do |driverId|        
    driver = get_driver_by_id(driverId)
    achievements = driver.achievement
    driverRides = get_rides_by_driver_id(driverId)
        
    if driverRides && achievements
        # Check FIVE_STAR_RIDE
        if !achievements.where(:key => "FIVE_STAR_RIDE").exists?
            if driverRides.where(:rating.gt => 95).exists?
                achievements << Achievement.new(key: "FIVE_STAR_RIDE", date: DateTime.now.to_time.utc)
            end
        end

        # Check GREAT_TEN
        if !achievements.where(:key => "GREAT_TEN").exists?
            greatRidesCount = driverRides.where(:rating.gt => 95).count
            if greatRidesCount >= 10
                achievements << Achievement.new(key: "GREAT_TEN", date: DateTime.now.to_time.utc)
            end
        end

        # Check FIVE_RIDES_PROGRESS
        if !achievements.where(:key => "FIVE_RIDES_PROGRESS").exists?
            lastRides = driverRides.where(finished: true).sort(date: -1).limit(5)
            if lastRides.size > 5
                status = true
                lastRating = lastRides.first.rating
                lastRides.each do |ride|
                    if lastRating < ride.rating
                        status = false
                        break
                    end
                end
                if status == true
                    achievements << Achievement.new(key: "FIVE_RIDES_PROGRESS", date: DateTime.now.to_time.utc)
                end
            end
        end

        # Check GREAT_HUNDRED
        if !achievements.where(:key => "GREAT_HUNDRED").exists?
            greatRidesCount = driverRides.where(:rating.gt => 95).count
            if greatRidesCount >= 100
                achievements << Achievement.new(key: "GREAT_HUNDRED", date: DateTime.now.to_time.utc)
            end
        end

        # Check BIRTHDAY_WORKING
        if !achievements.where(:key => "BIRTHDAY_WORKING").exists?
            birthday = driver.birthday
            if birthday != nil && birthday == Date.today
                achievements << Achievement.new(key: "BIRTHDAY_WORKING", date: DateTime.now.to_time.utc)
            end
        end

        # Check THOUSAND_WORKING_HOURS
        if !achievements.where(:key => "THOUSAND_WORKING_HOURS").exists?
            if (driver.total_duration / 3600000) >= 1000
                achievements << Achievement.new(key: "THOUSAND_WORKING_HOURS", date: DateTime.now.to_time.utc)
            end
        end
        status 201
    end
end