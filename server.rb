require 'sinatra'
require 'sinatra/namespace'
require 'open-uri'
require './models/models'
require './serializers/serializers'
require './helpers/helpers'

# DB Setup
Mongoid.load!("mongoid.yml", :development)

# Main entry point of Sinatra REST API application
class RestApi < Sinatra::Base
    register Sinatra::Namespace
    
    before do
        content_type 'application/json'
    end
    helpers RestApiHelpers
    require_relative './routes/init'
end