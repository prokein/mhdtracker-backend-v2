# module with all helpers used to transform data from requests and find data in MongoDB database by mongoid driver by MongoDB written in Ruby
module RestApiHelpers

    # generates base url
    #
    # @return URL
    def base_url
        @base_url ||= "#{request.env['rack.url_scheme']}://#{request.env['HTTP_HOST']}"
    end
    
    # parse the JSON request body document source into a Ruby data structure and return it.
    #
    # @return [Object] Ruby data structure from request body
    def json_params
        begin
            JSON.parse(request.body.read)
        rescue
            halt 400, { message:'Invalid JSON' }.to_json
        end
    end

    # finds driver by id in mongoDB database
    #
    # @param id [String] unique identificator of driver
    # @return [Driver] Driver object with all fields
    def get_driver_by_id(id)
        begin
            Driver.find(id)
        rescue Mongoid::Errors::DocumentNotFound
            halt(404, { message: "Driver with ID: #{id} not found"}.to_json)
        end
    end

    # finds ride by id in mongoDB database
    #
    # @param id [String] unique identificator of ride
    # @return [Ride] Ride object with all fields
    def get_ride(id)
        begin
            Ride.find(id)
        rescue Mongoid::Errors::DocumentNotFound
            halt(404, { message: "Ride with ID: #{id} not found"}.to_json)
        end
    end

    # finds rides by of driver in mongoDB database
    #
    # @param driver_id [String] unique identificator of driver
    # @return [Array<Ride>] array of Ride objects with all fields
    def get_rides_by_driver_id(driver_id)
        begin
            Ride.where(driver_id: driver_id)
        rescue Mongoid::Errors::DocumentNotFound
            halt(404, {message: "Rides of driver ID: #{id} not found"}.to_json)
        end
    end    
end