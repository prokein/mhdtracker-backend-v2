# Data serializer to get structured Achievement object - parse Achievement document from MongoDB database to specified structure
class AchievementSerializer  
  # AchievementSerializer constructor
  #
  # @param achievement [Achievement] Achievement model object
  # @return [AchievementSerializer]
  def initialize(achievement)
    @achievement = achievement
  end

  # function to convert [Achievement] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
    data = {
        date:@achievement["date"],
		key:@achievement["key"]
    }
    data
  end
end

# Data serializer to get structured Acceleration data object - parse acceleration array field from Waypoint document from MongoDB database to specified structure
class AccelerationSerializer  
  # AccelerationSerializer constructor
  #
  # @param acceleration [Acceleration] Acceleration model object
  # @return [AccelerationSerializer]
  def initialize(acceleration)
    @acceleration = acceleration
  end

  # function to convert [Acceleration] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
      if (@acceleration != nil)
        data = {
            a_x:@acceleration["accX"],
		    a_y:@acceleration["accY"],
		    a_z:@acceleration["accZ"]
        }
        data
      end
  end
end

# Data serializer to get structured Gyroscope data object - parse gyroscope array field from Waypoint document from MongoDB database to specified structure
class GyroSerializer
  # GyroSerializer constructor
  #
  # @param gyro [Gyro] Gyro model object
  # @return [GyroSerializer]  
  def initialize(gyro)
    @gyro = gyro
  end

  # function to convert [Gyro] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
    if(@gyro != nil)
        data = {
            g_x:@gyro["gyroX"],
            g_y:@gyro["gyroY"],
            g_z:@gyro["gyroZ"]
        }
        data
    end
  end
end

# Data serializer to get structured Location data object - parse location array field from Waypoint document from MongoDB database to specified structure
class LocationSerializer
  # LocationSerializer constructor
  #
  # @param location [Location] Location model object
  # @return [LocationSerializer] 
  def initialize(location)
    @location = location
  end

  # function to convert [Location] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
    if(@location != nil)
        data = {
            a:@location["altitude"],
            b:@location["bearing"],
            la:@location["latitude"],
            lo:@location["longitude"],
            s:@location["speed"]
        }
    data
    end
  end
end

# Data serializer to get structured Waypoint object - parse Waypoint document from MongoDB database to specified structure
class WaypointSerializer
    # WaypointSerializer constructor
    #
    # @param waypoint [Waypoint] Waypoint model object
    # @return [WaypointSerializer]   
    def initialize(waypoint)
        @waypoint = waypoint
    end

    # function to convert [Waypoint] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
		acceleration = AccelerationSerializer.new(@waypoint['acceleration'])
		gyro = GyroSerializer.new(@waypoint['gyro'])
		location = LocationSerializer.new(@waypoint['location'])
        data = {
            n:@waypoint['no'],
			r:@waypoint['rating'],
			t:@waypoint['timestamp'],
			t_a:@waypoint['total_acceleration'],
			a:acceleration,
			g:gyro,
			l:location
        }
        data
    end
end

# Data serializer to get structured Ride object - parse Ride document from MongoDB database to specified structure
class RideSerializer
    # RideSerializer constructor
    #
    # @param ride [Ride] Ride model object
    # @return [RideSerializer]     
    def initialize(ride)
        @ride = ride
    end

    # function to convert [Ride] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
        data = {
            id:@ride.id,
            s:@ride.service,
            l:@ride.link,
            dir:@ride.direction,
            date:@ride.date,
            dur:@ride.duration,
            dis:@ride.distance,
            max_s:@ride.max_speed,
            avg_s:@ride.avg_speed,
            r:@ride.rating
        }
        data[:errors] = @ride.errors if@ride.errors.any?
        data
    end
end

# Data serializer to get structured Driver object - parse Driver document from MongoDB database to specified structure
class DriverSerializer
  # DriverSerializer constructor
  #
  # @param driver [Driver] Driver model object
  # @return [DriverSerializer]   
  def initialize(driver)
    @driver = driver
  end

  # function to convert [Driver] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
    data = {
        id:@driver.id,
        name:@driver.name,
        avg_speed:@driver.avg_speed,
        max_speed:@driver.max_speed,
		rating:@driver.rating,
        no_rides:@driver.no_rides,
        total_distance:@driver.total_distance,
        total_duration:@driver.total_duration,
		achievement:@driver.achievement
    }
    data[:errors] = @driver.errors if@driver.errors.any?
    data
  end
end

# Data serializer to get basic statistic information from Driver object - parse Driver document from MongoDB database to specified structure
class DriverStatsSerializer
  # DriverStatsSerializer constructor
  #
  # @param driver [Driver] Driver model object
  # @return [DriverStatsSerializer] 
  def initialize(driver)
    @driver = driver
  end

  # function to convert [Driver] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
    data = {
        avg_speed:@driver.avg_speed,
        max_speed:@driver.max_speed,
		rating:@driver.rating,
        no_rides:@driver.no_rides,
        total_distance:@driver.total_distance,
        total_duration:@driver.total_duration,
    }
    data
  end
end

# Data serializer to get basic information from Driver object - parse Driver document from MongoDB database to specified structure
class DriverInfoSerializer
  # DriverInfoSerializer constructor
  #
  # @param driver [Driver] Driver model object
  # @return [DriverInfoSerializer]   
  def initialize(driver)
    @driver = driver
  end

  # function to convert [Driver] model object to specified JSON object
  #
  # @return [JSON]
  def as_json(*)
    data = {
        id:@driver.id,
        name:@driver.name,
        pictureUrl:@driver.pictureUrl
    }
    data
  end
end

# Data serializer to get basic information from Ride object - parse Ride document from MongoDB database to specified structure
class RideInfoSerializer
    # RideInfoSerializer constructor
    #
    # @param ride [Ride] Ride model object
    # @return [RideInfoSerializer]    
    def initialize(ride)
        @ride = ride
    end
    
    # function to convert [Ride] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
        data = {
            rideId:@ride.id,
            version:@ride.version,
            date:@ride.date,
            service:@ride.service,
            direction:@ride.direction,
            link:@ride.link
        }
        data[:errors] = @ride.errors if@ride.errors.any?
        data
    end
end

# Data serializer to get basic statistic information from Ride object - parse Ride document from MongoDB database to specified structure
class RideStatsSerializer
    # RideStatsSerializer constructor
    #
    # @param ride [Ride] Ride model object
    # @return [RideStatsSerializer]   
    def initialize(ride)
        @ride = ride
    end
    
    # function to convert [Ride] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
        data = {
            avg_speed:@ride.avg_speed,
            max_speed:@ride.max_speed,
            duration:@ride.duration,
            distance:@ride.distance,
            rating:@ride.rating
        }
        data[:errors] = @ride.errors if@ride.errors.any?
        data
    end
end

# Data serializer to get structured Link object - parse Link document from MongoDB database to specified structure
class LinkSerializer
    # LinkSerializer constructor
    #
    # @param link [Link] Link model object
    # @return [LinkSerializer]   
    def initialize(link)
        @link = link
    end
    
    # function to convert [Link] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
        data = {
            id:@link.id,
            name:@link.name,
            zd:@link.zeroDirection,
            od:@link.oneDirection
        }
        data[:errors] = @link.errors if@link.errors.any?
        data
    end
end

# Data serializer to get structured data for Ride chart object - parse Ride document from MongoDB database to specified structure
class RideChartSerializer
    # RideChartSerializer constructor
    #
    # @param ride [Ride] Ride model object
    # @return [RideChartSerializer]  
    def initialize(ride)
        @ride = ride
    end

    # function to convert [Ride] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
        data = {
            date:@ride.date,
            rating:@ride.rating
        }
        data[:errors] = @ride.errors if@ride.errors.any?
        data
    end
end

# Data serializer to get structured Warning object - parse Warning document from MongoDB database to specified structure
class WarningSerializer
    # WarningSerializer constructor
    #
    # @param warning [Ridewarning] Ridewarning model object
    # @return [WarningSerializer]   
    def initialize(warning)
        @warning = warning
    end

    # function to convert [Ridewarning] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
        waypoint = WaypointSerializer.new(@warning['waypoint'])
        data = {
            id:@warning.id,
            ride_id:@warning.ride_id,
            key:@warning.key,
            waypoint:waypoint
        }
        data[:errors] = @warning.errors if@warning.errors.any?
        data
    end
end

# Data serializer to get structured analyze Settings object - parse Analyzesetting document from MongoDB database to specified structure
class AnalyzeSettingsSerializer
    # AnalyzeSettingsSerializer constructor
    #
    # @param settings [Analyzesetting] Analyzesettings model object
    # @return [AnalyzeSettingsSerializer]   
    def initialize(settings)
      @settings = settings
    end
    
    # function to convert [Analyzesetting] model object to specified JSON object
    #
    # @return [JSON]
    def as_json(*)
      data = {
          version:@settings.version,
          gps_accuracy:@settings.gps_accuracy,
          location_update_time:@settings.location_update_time,
          location_update_dist:@settings.location_update_dist,
          low_pass_filter:@settings.low_pass_filter,
          alpha_low_pass:@settings.alpha_low_pass,
          use_average:@settings.use_average,
          bus_max_acc:@settings.bus_max_acc,
          bus_min_acc:@settings.bus_min_acc,
          bus_max_break:@settings.bus_max_break,
          bus_min_break:@settings.bus_min_break,
          bus_max_turn:@settings.bus_max_turn,
          bus_min_turn:@settings.bus_min_turn,
          bus_min_gyro_turn:@settings.bus_min_gyro_turn,
          tram_max_acc:@settings.tram_max_acc,
          tram_min_acc:@settings.tram_min_acc,
          tram_max_break:@settings.tram_max_break,
          tram_min_break:@settings.tram_min_break,
          tram_max_turn:@settings.tram_max_turn,
          tram_min_turn:@settings.tram_min_turn,
          tram_min_gyro_turm:@settings.tram_min_gyro_turn
      }
      data[:errors] = @settings.errors if@settings.errors.any?
      data
    end
end