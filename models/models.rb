require 'mongoid'


# Acceleration data model object
#
# @attr [Float] accX - accelerometer value in x axis
# @attr [Float] accY - accelerometer value in y axis
# @attr [Float] accZ - accelerometer value in z axis
#
# @example
#   {
#     "accX": -0.0106321296540316,
#     "accY": -0.170906977912949,
#     "accZ": -0.0189299770275922
#   }
class Acceleration
    include Mongoid::Document
    field :accX, type: Float
    field :accY, type: Float
    field :accZ, type: Float
    embedded_in :waypoint
end

# Gyro data model object
#
# @attr [Float] gyroX - gyroscope value in x axis
# @attr [Float] gyroY - gyroscope value in y axis
# @attr [Float] gyroZ - gyroscope value in z axis
#
# @example
#   {
#     "gyroX": -0.0138971842825413,
#     "gyroY": 0.00244346098043025,
#     "gyroZ": 0.103541657328606
#   }
class Gyro
    include Mongoid::Document
    field :gyroX, type: Float
    field :gyroY, type: Float
    field :gyroZ, type: Float
    embedded_in :waypoint
end

# Location data model object
#
# @attr [Float] altitude - gps location altitude value
# @attr [Float] bearing - gps location bearing value (degrees)
# @attr [Float] latitude - gps location latitude value
# @attr [Float] longitude - gps location longitude value
# @attr [Float] speed - gps location speed value (m/s)
#
# @example
#   {
#     "altitude": 319.273106105654,
#     "bearing": 307.852325439453,
#     "latitude": 48.7603886056268,
#     "bearing": 21.2601002698786s,
#     "latitude": 7.79403495788574
#   }
class Location
    include Mongoid::Document
    field :altitude, type: Float
    field :bearing, type: Float
    field :latitude, type: Float
    field :longitude, type: Float
    field :speed, type: Float
    embedded_in :waypoint
end
 
# Waypoint data model object
#
# @attr [String] ride_id - ride identificator
# @attr [Integer] no - order number of waypoint
# @attr [Float] rating - calculated rating for waypoint (percent)
# @attr [Integer] timestamp - actual time from monitoring start (miliseconds)
# @attr [Float] total_acceleration - calculated total acceleration for waypoint
# @attr [Acceleration] acceleration - Acceleration object with all it's fields
# @attr [Gyro] gyro - Gyro object with all it's fields
# @attr [Location] location - Location object with all it's fields
#
# @example
#   {
#     "ride_id": "123654-27-0-2017-03-09T09:17:40.962Z",
#     "no": 0,
#     "rating": 100.0,
#     "timestamp": 50,
#     "total_acceleration": 0.173588916908246,
#     "acceleration": {
#           "accX": -0.0106321296540316,
#           "accY": -0.170906977912949,
#           "accZ": -0.0189299770275922
#     },
#     "gyro": {
#           "gyroX": -0.0138971842825413,
#           "gyroY": 0.00244346098043025,
#           "gyroZ": 0.103541657328606 
#     }, 
#     "location": {
#           "altitude": 319.273106105654,
#           "bearing": 307.852325439453,
#           "latitude": 48.7603886056268,
#           "bearing": 21.2601002698786s,
#           "latitude": 7.79403495788574 
#     }
#   }
class Waypoint
    include Mongoid::Document
    field :ride_id, type: String
    field :no, type: Integer
    field :rating, type: Float
    field :timestamp, type: Integer
    field :total_acceleration, type: Float
    embeds_one :acceleration
    embeds_one :gyro
    embeds_one :location
end

# Ridewarning data model object
#
# @attr [String] key - value of enumeration type of current Ridewarning event
# @attr [String] ride_id - ride identificator
# @attr [Boolean] reviewed - is true when dispatcher reviewed this warning - default value false
# @attr [Waypoint] waypoint - Waypoint object with location and basic stats
#
# @example
#   {
#     "key": "RIGHT_TURN",
#     "ride_id": "1123-5-1-2017-04-14T13:45:25.453Z",
#     "reviewed": true,
#     "waypoint": {
#           "l": {
#               "a": 333.484445055661,
#               "b": 0.0,
#               "la": 48.7965775138502,
#               "lo": 21.3923073261662,
#               "s": 0.0
#           },
#           "t_a": 0.0,
#           "t": 0,
#           "r": 47.0956420898438,
#           "n": 0
#     }
#   }
class Ridewarning
    include Mongoid::Document
    field :key, type: String
    field :ride_id, type: String 
    field :reviewed, type: Boolean, default: false
    field :waypoint, type: Waypoint
    validates :key, presence: true
end

# Ride data model object
#
# @attr [String] id - ride identificator
# @attr [String] driver_id - driver identificator
# @attr [DateTime] date - date time stamp when ride monitoring starts
# @attr [String] link - link name for ride
# @attr [Boolean] direction - direction of link
# @attr [String] service - service identificator
# @attr [Float] avg_speed - calculated average speed for actual ride (m/s)
# @attr [Float] max_speed - calculated max speed for actual ride (m/s)
# @attr [Float] rating - calculated total rating for ride (percent)
# @attr [Integer] version - analyze settings for pattern modulation version
# @attr [Float] distance - calculated distance, which vehicle takes during monitoring (m)
# @attr [Integer] duration - duration of monitoring for current ride (miliseconds)
# @attr [Boolean] finished - boolean checker if ride is finished
# @attr [Boolean] online - boolean checker if ride is online
#
# @example
#   {
#     "id": "123654-27-0-2017-03-09T09:17:40.962Z",
#     "driver_id": "qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#     "date": "2017-03-09 09:17:40.962Z",
#     "link": "27",
#     "direction": false,
#     "service": "123654",
#     "avg_speed": 12.8244980055848,
#     "max_speed": 19.0897579193115,
#     "rating": 99.8442369269885,
#     "version": 1,
#     "distance": 5622.22681903839,
#     "duration": 575581,
#     "finished": true,
#     "online": false
#   }
class Ride
    include Mongoid::Document
    field :id, type: String
    field :driver_id, type: String
    field :date, type: DateTime
    field :link, type: String
    field :direction, type: Boolean
    field :service, type: String
    field :avg_speed, type: Float
    field :max_speed, type: Float
    field :rating, type: Float
    field :version, type: Integer
    field :distance, type: Float
    field :duration, type: Integer
    field :finished, type: Boolean, default: false
    field :online, type: Boolean, default: false
end

# Achievement data model object
#
# @attr [String] key - value of enumeration type key for achieved Achievement
# @attr [DateTime] date - time, when driver achieved Achievement
#
# @example
#   {
#     "key": "FIVE_STAR_RIDE",
#     "date": "2017-04-02 09:51:36.021Z"
#   }
class Achievement
    include Mongoid::Document
    field :key, type: String
    field :date, type: DateTime
    embedded_in :driver
end

# Driver data model object
#
# @attr [String] id - driver identificator
# @attr [String] name - driver full name
# @attr [Date] birthday - driver's birthday
# @attr [String] token - driver's instance of application token
# @attr [Float] avg_speed - calculated total average speed for driver from all his rides (m/s)
# @attr [Float] max_speed - calculated total max speed for driver from all his rides (m/s)
# @attr [Float] rating - calculated total rating for driver from all his rides (percent)
# @attr [Integer] no_rides - number of monitored rides for driver
# @attr [Float] total_distance - total distance of all monitored rides, which driver takes (m)
# @attr [Integer] total_duration - total duration of all monitored rides, which driver takes (milisecond)
# @attr [String] picture - driver profile picture identificator 
# @attr [String] pictureUrl - download url for dirver profile picture
# @attr [Array(String)] achievement - array of all achievements, which driver achieves
#
# @example
#   {
#     "id": "qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#     "name": "Lukáš Prokein",
#     "birthday": "1995-03-21 23:00:00.000Z",
#     "token": "ewNM9eyFJFw:APA91bF_m9htJLxduKYz2eT5cqtVEY2TBT8z0PFYdBztzo2YZHaexCqjGCGMJSEQXflOWZOXbUHDYVFbgAFpd81pm61AcnB-VQfmR8386ZORR-8HD3Q939RomJCKyYrdB0Skhx1BvhTr",
#     "avg_speed": 2.61625744044793,
#     "max_speed": "23.2004070281982",
#     "rating": 83.0968325142815,
#     "no_rides": 36.0,
#     "total_distance": 60633.0,
#     "total_duration": 10047203.0,
#     "picture": "ad571489-71a8-ed79-9cb2-26ed58b56282",
#     "pictureUrl": "https://firebasestorage.googleapis.com/v0/b/mhdtracker-c3fd3.appspot.com/o/users%2Fad571489-71a8-ed79-9cb2-26ed58b56282?alt=media&token=68abbbf2-53ca-447f-beca-68a5a6db9538",
#     "achievement": [
#           {
#               "key": "FIVE_STAR_RIDE",
#               "date": "2017-04-02 09:51:36.021Z"
#           },
#           {
#               "key": "FIVE_RIDES_PROGRESS",
#               "date": "2017-04-04 11:25:31.353Z"
#           }
#     ]
#   }
class Driver
    include Mongoid::Document
    field :id, type: String
    field :name, type: String
    field :birthday, type: Date
    field :token, type: String
    field :avg_speed, type: Float, default: 0
    field :max_speed, type: Float, default: 0
    field :rating, type: Float, default: 100
    field :no_rides, type: Integer, default: 0
    field :total_distance, type: Float, default: 0
    field :total_duration, type: Integer, default: 0
    field :picture, type: String
    field :pictureUrl, type: String
    embeds_many :achievement

    validates :id, presence: true
    validates :name, presence: true
end

# Link data model object
#
# @attr [String] id - link identificator
# @attr [String] name - link name
# @attr [String] zeroDirection - zero direction title
# @attr [String] oneDirection - one direction title
#
# @example
#   {
#     "id": "20013",
#     "name": "2",
#     "zeroDirection": "Stani\u010dn\u00e9 n\u00e1mestie",
#     "oneDirection": "Havl\u00ed\u010dkova"
#   }
class Link
    include Mongoid::Document
    field :id, type: String
    field :name, type: String
    field :zeroDirection, type: String
    field :oneDirection, type: String
end

# Analyzesetting data model object
#
# @attr [Integer] version - number of settings version
# @attr [Integer] gps_accuracy - gps accuracy radius (m)
# @attr [Integer] location_update_time - time interval for requesting lcoation from gps sensor (miliseconds)
# @attr [Integer] location_update_dist - distance interval for requesting location from gps sensor (m)
# @attr [Boolean] low_pass_filter - if true low pass filter is enabled, if true disabled
# @attr [Float] alpha_low_pass - koeficient for intensity of low pass filter
# @attr [Boolean] use_average - if true algorithm uses average values of calculated acceleration, otherwise uses peaks
# @attr [Float] bus_max_acc - maximum koeficient value of unconfortable acceleration event identification for bus
# @attr [Float] bus_min_acc - minimum koeficient value of unconfortable acceleration event identification for bus
# @attr [Float] bus_max_break - maximum koeficient value for unconfortable breaking event identification for bus
# @attr [Float] bus_min_break - minimum koeficient value for unconfortable breaking event identification for bus
# @attr [Float] bus_max_turn - maximum koeficient value for unconfortable turning event identification for bus
# @attr [Float] bus_min_turn - minimum koeficient value for unconfortable turning event identification for bus
# @attr [Float] bus_min_gyro_turn - minimum koeficient value for gyroscope turning calculation for bus
# @attr [Float] tram_max_acc - maximum koeficient value of unconfortable acceleration event identification for tram
# @attr [Float] tram_min_acc - minimum koeficient value of unconfortable acceleration event identification for tram
# @attr [Float] tram_max_break - maximum koeficient value for unconfortable breaking event identification for tram
# @attr [Float] tram_min_break - minimum koeficient value for unconfortable breaking event identification for tram
# @attr [Float] tram_max_turn - maximum koeficient value for unconfortable turning event identification for tram
# @attr [Float] tram_min_turn - minimum koeficient value for unconfortable turning event identification for tram
# @attr [Float] tram_min_gyro_turn - minimum koeficient value for gyroscope turning calculation for tram
# @attr [DateTime] date - date and time when settings were created
#
# @example
#   {
#     "version": 1,
#     "gps_accuracy": 18,
#     "location_update_time": 1000,
#     "location_update_dist": 5,
#     "low_pass_filter": false,
#     "alpha_low_pass": 0.25,
#     "use_average": false,
#     "bus_max_acc": 4,
#     "bus_min_acc": 1,
#     "bus_max_break": 5,
#     "bus_min_break": 1.5,
#     "bus_max_turn": 4,
#     "bus_min_turn": 1.5,
#     "bus_min_gyro_turn": 0.22,
#     "tram_max_acc": 4,
#     "tram_min_acc": 1,
#     "tram_max_break": 5,
#     "tram_min_break": 1.5,
#     "tram_max_turn": 4,
#     "tram_min_turn": 1.5,
#     "tram_min_gyro_turn": 0.22,
#     "date": "2017-04-15 13:26:17.295Z" 
#   }
class Analyzesetting
    include Mongoid::Document
    field :version, type: Integer
    field :gps_accuracy, type: Integer
    field :location_update_time, type: Integer
    field :location_update_dist, type: Integer
    field :low_pass_filter, type: Boolean
    field :alpha_low_pass, type: Float
    field :use_average, type: Boolean
    field :bus_max_acc, type: Float
    field :bus_min_acc, type: Float
    field :bus_max_break, type: Float
    field :bus_min_break, type: Float
    field :bus_max_turn, type: Float
    field :bus_min_turn, type: Float
    field :bus_min_gyro_turn, type: Float
    field :tram_max_acc, type: Float
    field :tram_min_acc, type: Float
    field :tram_max_break, type: Float
    field :tram_min_break, type: Float
    field :tram_max_turn, type: Float
    field :tram_min_turn, type: Float
    field :tram_min_gyro_turn, type: Float
    field :date, type: DateTime
end

# Report data model object
#
# @attr [Float] overflown_rating - rating value, which is not in interval bertween 0 to 100 percent
# @attr [Waypoint] waypoint - Waypoint object with all it's fieldss
# @attr [String] driver_id - driver identificator
# @attr [String] ride_id - ride identificator
# @attr [String] event_key - value from enumeration type for RideWarning
#
# @example
#   {
#     "overflown_rating": -15.28046471732,
#     "waypoint": {
#           "a": {
#               "a_x": -0.797103226184845,
#               "a_y": 1.18211483955383,
#               "a_z": -5.5348162651062
#           },
#           "g": {
#               "g_x": 0.0406225398182869,
#               "g_y": 0.235793977975845,
#               "g_z": 0.0207694191485643
#           },
#           "l": {
#               "a": 281.095770160916,
#               "b": 68.600341796875,
#               "la": 48.6973534194638,
#               "lo": 21.2321458192649,
#               "s": 10.0338907241821
#           },
#           "t_a": 5.7155017396176,
#           "t": 358341,
#           "r": 0.0,
#           "n": 300
#     },
#     "driver_id": "qz9s6qaVmXQw1kMyrmlbpGXVmHB2",
#     "ride_id": "3908-10-0-2017-05-11T15:35:44.122Z",
#     "event_key": "BREAKING"
#   }
class Report
    include Mongoid::Document
    field :overflown_rating, type: Float
    field :waypoint, type: Waypoint
    field :driver_id, type: String
    field :ride_id, type: String
    field :event_key, type: String
end